package ud.bolsaempleo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BolsaEmpleoUdBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(BolsaEmpleoUdBackEndApplication.class, args);
	}
}
