package ud.bolsaempleo.util;

public class JsonNumber {
	private int num;

	public JsonNumber(int num) {
		this.num = num;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
	
	
}
