package ud.bolsaempleo.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import ud.bolsaempleo.model.Estudio;
import ud.bolsaempleo.model.Experiencia;
import ud.bolsaempleo.model.Oferta;
import ud.bolsaempleo.model.Usuario;

@PropertySource(value = { "classpath:application.yml" })
public class EnvioMail {
	
    @Autowired
    private Environment environment;
	
	private String username;
	private String password;
	private String smtpHost;
	private String port;
	private String starttls;
	private String smtpAuth;
	Properties props;

	public EnvioMail() {
		props = new Properties();
		username = environment.getRequiredProperty("emails.username");
		password = environment.getRequiredProperty("emails.password");
		smtpHost = environment.getRequiredProperty("emails.smtpHost");
		port = environment.getRequiredProperty("emails.port");
		starttls = environment.getRequiredProperty("emails.starttls");
		smtpAuth = environment.getRequiredProperty("emails.smtpAuth");
	}

	public String mailAplicacionEmpresa(Usuario usuario, List<Experiencia> experiencias, List<Estudio> estudios, Oferta oferta, Usuario empresa, File archivo,Environment environment) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		

		props.put("mail.smtp.starttls.enable", starttls);
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.auth", smtpAuth);

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			String htmlBody;

			htmlBody = "<html><head><meta charset=\"UTF-8\"><title>Bolsa de Empleo UD</title>"
					+ "</head><body>"
					+ "<h2>Bolsa de Empleo UD - Nueva Hoja de vida</h2>"
					+ "<h3>El aspirante "+usuario.getNombre()+" aplicó al cargo: "+oferta.getCargo()+"</h3>"
					+"<hr><hr><h2>HOJA DE VIDA</h2>"
					+"<div><label style=\"font-weight: bold\">Nombre:</label><p>"+usuario.getNombre()+"</p></div>"
				    + "<div><label style=\"font-weight: bold\">Correo Electrónico:</label><p>"+usuario.getEmail()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Tipo Documento:</label><p>"+usuario.getTipoDoc()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Numero Documento:</label><p>"+usuario.getDoc()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Teléfono:</label><p>"+usuario.getTelefono()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Perfil:</label><p>"+usuario.getPerfil()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Pais:</label><p>"+usuario.getPais()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Ciudad:</label><p>"+usuario.getCiudad()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Numero Libreta Militar:</label><p>"+usuario.getLibreta()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Dirección:</label><p>"+usuario.getDireccion()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Pagina web:</label><p>"+usuario.getWeb()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Genero:</label><p>"+usuario.getGenero()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Estado Civil:</label><p>"+usuario.getEstadoCivil()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Categoria Licencia de Conducción:</label><p>"+usuario.getCatLicenciaCond()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Numero Licencia de Conducción:</label><p>"+usuario.getLicenciaCond()+"</p></div>";
				    
			htmlBody =htmlBody+ "<hr><hr><h2>LOGROS ACADÉMICOS</h2>";
			for (Estudio estudio : estudios) {
				htmlBody = htmlBody + "<div><label style=\"font-weight: bold\">Nombre:</label><p>"+estudio.getNombre()+"</p></div>";
				htmlBody = htmlBody + "<div><label style=\"font-weight: bold\">Inicio / Fin:</label><p>"+dateFormat.format(estudio.getInicio())+" / "+dateFormat.format(estudio.getTerminacion())+"</p></div>";
				htmlBody = htmlBody + "<div><label style=\"font-weight: bold\">Tipo:</label><p>"+estudio.getTipo()+"</p></div>";
				htmlBody = htmlBody + "<div><label style=\"font-weight: bold\">Institución:</label><p>"+estudio.getInstitucion()+"</p></div><hr>";
			}
			htmlBody =htmlBody+ "<hr><h2>EXPERIENCIA LABORAL</h2>";
			for (Experiencia experiencia : experiencias) {
				htmlBody = htmlBody + "<div><label style=\"font-weight: bold\">Empresa:</label><p>"+experiencia.getEmpresa()+"</p></div>";
				htmlBody = htmlBody + "<div><label style=\"font-weight: bold\">Inicio / Fin:</label><p>"+dateFormat.format(experiencia.getFechaInicio())+" / "+dateFormat.format(experiencia.getFechaFin())+"</p></div>";
				htmlBody = htmlBody + "<div><label style=\"font-weight: bold\">Cargo:</label><p>"+experiencia.getCargo()+"</p></div>";
				htmlBody = htmlBody + "<div><label style=\"font-weight: bold\">Contacto:</label><p>"+experiencia.getContacto()+"</p></div>";
				htmlBody = htmlBody + "<div><label style=\"font-weight: bold\">Telefono Contacto:</label><p>"+experiencia.getTelContacto()+"</p></div>";
				htmlBody = htmlBody + "<div><label style=\"font-weight: bold\">Funciones:</label><p>"+experiencia.getFunciones()+"</p></div><hr>";
			}	    
				    
			htmlBody =htmlBody+ "</body></html>";

			Message msg = new MimeMessage(session);

			Multipart mp = new MimeMultipart();

			msg.setFrom(new InternetAddress(username,"Bolsa de Empleo UD"));
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(empresa.getEmail()));

			msg.setSubject("Aplicacion a oferta laboral:" + oferta.getCargo());

			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(htmlBody, "text/html");
			mp.addBodyPart(htmlPart);

			if(archivo!=null){
				MimeBodyPart attachment = new MimeBodyPart();
				attachment.setFileName("Hoja de vida"+archivo.getName());
				attachment.setDataHandler(new DataHandler(new FileDataSource(
					environment.getRequiredProperty("rutas.adjuntos") + "/"+archivo.getName())));
				mp.addBodyPart(attachment);
			}

			msg.setContent(mp);
			Transport.send(msg);

			return "Correo Enviado";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}
	
	public String mailAplicacionOferta(Usuario usuario, Oferta oferta, Usuario empresa) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			String htmlBody;

			htmlBody = "<html><head><meta charset=\"UTF-8\"><title>Bolsa de Empleo UD</title>"
					+ "</head><body>"
					+ "<h2>Bolsa de Empleo UD - Aplicación a Oferta</h2>"
					+ "<h3>Usted aplico correctamente a la Oferta Laboral de la Empresa: "+empresa.getNombre()+"</h3>"
					+"<div><label style=\"font-weight: bold\">Cargo:</label><p>"+oferta.getCargo()+"</p></div>"
				    + "<div><label style=\"font-weight: bold\">Salario:</label><p> $"+oferta.getSalario()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Fecha de creación:</label><p>"+dateFormat.format(oferta.getFechaInicial())+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Fecha de cierre:</label><p>"+dateFormat.format(oferta.getFechaFinal())+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Pais:</label><p>"+oferta.getPais()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Ciudad:</label><p>"+oferta.getCiudad()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Descripción:</label><p>"+oferta.getDescripcion()+"</p></div>"
				    +"<div><label style=\"font-weight: bold\">Empresa:</label><p>"+empresa.getNombre()+"</p></div>"
					+ "</body></html>";

			Message msg = new MimeMessage(session);

			Multipart mp = new MimeMultipart();

			msg.setFrom(new InternetAddress(username,"Bolsa de Empleo UD"));
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(usuario.getEmail()));

			msg.setSubject("Aplicacion a oferta laboral:" + oferta.getCargo());

			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(htmlBody, "text/html");
			mp.addBodyPart(htmlPart);

			msg.setContent(mp);
			Transport.send(msg);

			return "Correo Enviado";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}
	
	public String mailRecuperarPassword(Usuario usuario) {

		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			String htmlBody;

			htmlBody = "<html><head><meta charset=\"UTF-8\"><title>Bolsa de Empleo UD</title>"
					+ "</head><body>"
					+ "<h2>Bolsa de Empleo UD - Recuperación de contraseña</h2>"
					+ "<h3>Hola! "+usuario.getNombre()+" tu nueva contraseña es: "+usuario.getPass()+"</h3>"
					+ "<h3>No olvides cambiarla en tu perfil de la aplicación cuando ingreses.</p></h3>"
					+ "</body></html>";

			Message msg = new MimeMessage(session);

			Multipart mp = new MimeMultipart();

			msg.setFrom(new InternetAddress(username,"Bolsa de Empleo UD"));
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(usuario.getEmail()));

			msg.setSubject("Recuperación de contraseña");

			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(htmlBody, "text/html");
			mp.addBodyPart(htmlPart);

			msg.setContent(mp);
			Transport.send(msg);

			return "Correo Enviado";

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

}
