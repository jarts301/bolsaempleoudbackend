package ud.bolsaempleo.util;

public class HojaVidaFile {
	
	private String nombre;
	private String extension;
	
	public HojaVidaFile(String nombre, String extension) {
		this.nombre = nombre;
		this.extension = extension;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extencion) {
		this.extension = extencion;
	}

}
