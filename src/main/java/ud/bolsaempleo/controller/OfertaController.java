package ud.bolsaempleo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ud.bolsaempleo.EmptyJsonResponse;
import ud.bolsaempleo.model.Oferta;
import ud.bolsaempleo.service.OfertaService;
import ud.bolsaempleo.util.JsonNumber;

@RestController
public class OfertaController {
	
	@Autowired
	OfertaService service;
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/oferta_obtenerPorSector")
	public @ResponseBody ResponseEntity<?> obtenerPorSector(
			@RequestParam(required = true) Integer sectorId) {
		List<Oferta> respuesta = service.obtenerPorSector(sectorId);
		if (respuesta != null) {
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/oferta_obtenerPorId")
	public @ResponseBody ResponseEntity<?> obtenerPorId(
			@RequestParam(required = true) Integer ofertaId) {
		Oferta respuesta = service.obtenerPorId(ofertaId);
		if (respuesta != null) {
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/oferta_registrar")
	public @ResponseBody ResponseEntity<?> registrar(
			@RequestParam(required = true) String cargo,
			@RequestParam(required = true) String salario,
			@RequestParam(required = true) Integer sector,
			@RequestParam(required = true) String descripcion,
			@RequestParam(required = true) String fechaCierre,
			@RequestParam(required = true) String pais,
			@RequestParam(required = true) String ciudad,
			@RequestParam(required = true) Integer usuarioId
			) throws ParseException{
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		Oferta oferta = new Oferta();
		oferta.setCargo(cargo);
		oferta.setDescripcion(descripcion);
		oferta.setFechaInicial(new Date());
		oferta.setFechaFinal(dateFormat.parse(fechaCierre));
		oferta.setSalario(salario);
		oferta.setSectorId(sector);
		oferta.setUsuarioId(usuarioId);
		oferta.setPais(pais);
		oferta.setCiudad(ciudad);
		
		if(oferta.getFechaFinal().after(new Date())){
			oferta.setActiva(true);
		}else{
			oferta.setActiva(false);
		}
		
		Integer respuesta = service.registrar(oferta);
		
		if (respuesta != null) {
			return new ResponseEntity<>(new JsonNumber(respuesta), HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}

}
