package ud.bolsaempleo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ud.bolsaempleo.EmptyJsonResponse;
import ud.bolsaempleo.model.Estudio;
import ud.bolsaempleo.service.EstudioService;
import ud.bolsaempleo.util.JsonNumber;

@RestController
public class EstudioController {
	
	@Autowired
	EstudioService service;
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/estudio_obtenerPorUsuario")
	public @ResponseBody ResponseEntity<?> obtenerPorUsuario(
			@RequestParam(required = true) Integer usuarioId) {
		
		List<Estudio> respuesta = service.obtenerPorUsuario(usuarioId);
		if (respuesta != null) {
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/estudio_obtenerPorId")
	public @ResponseBody ResponseEntity<?> obtenerPorId(
			@RequestParam(required = true) Integer estudioId) {
		Estudio respuesta = service.obtenerPorId(estudioId);
		if (respuesta != null) {
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/estudio_registrar")
	public @ResponseBody ResponseEntity<?> registrar(
			@RequestParam(required = true) String nombre,
			@RequestParam(required = true) String inicio,
			@RequestParam(required = true) String terminacion,
			@RequestParam(required = true) String tipo,
			@RequestParam(required = true) String institucion,
			@RequestParam(required = true) Integer usuarioId
			) throws ParseException {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		Estudio estudio = new Estudio();
		estudio.setInstitucion(institucion);
		estudio.setNombre(nombre);
		estudio.setTipo(tipo);
		estudio.setUsuarioId(usuarioId);
		estudio.setInicio(dateFormat.parse(inicio));
		estudio.setTerminacion(dateFormat.parse(terminacion));
		
		Integer respuesta = service.registrar(estudio);
		
		if (respuesta != null) {
			return new ResponseEntity<>(new JsonNumber(respuesta), HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/estudio_eliminar")
	public @ResponseBody ResponseEntity<?> eliminar(
			@RequestParam(required = true) Integer estudioId){
		
		Estudio estudio = service.obtenerPorId(estudioId);
		service.eliminar(estudio);
		
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}

}
