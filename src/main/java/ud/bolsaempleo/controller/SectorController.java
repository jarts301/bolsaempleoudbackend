package ud.bolsaempleo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ud.bolsaempleo.EmptyJsonResponse;
import ud.bolsaempleo.model.Sector;
import ud.bolsaempleo.service.SectorService;

@RestController
public class SectorController {
	
	@Autowired
	SectorService service;
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/sector_obtenerPorId")
	public @ResponseBody ResponseEntity<?> obtenerPorId(
			@RequestParam(required = true) Integer seccionId) {
		Sector respuesta = service.obtenerPorId(seccionId);
		if (respuesta != null) {
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/sector_obtenerTodos")
	public @ResponseBody ResponseEntity<?> obtenerTodos() {
		List<Sector> respuesta = service.obtenerTodos();
		if (respuesta != null) {
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}

}
