package ud.bolsaempleo.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ud.bolsaempleo.EmptyJsonResponse;
import ud.bolsaempleo.model.Experiencia;
import ud.bolsaempleo.service.ExperienciaService;
import ud.bolsaempleo.util.JsonNumber;

@RestController
public class ExperienciaController {

	@Autowired
	ExperienciaService service;

	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/experiencia_obtenerPorUsuario")
	public @ResponseBody ResponseEntity<?> obtenerPorUsuario(@RequestParam(required = true) Integer usuarioId) {

		List<Experiencia> respuesta = service.obtenerPorUsuario(usuarioId);
		if (respuesta != null) {
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/experiencia_obtenerPorId")
	public @ResponseBody ResponseEntity<?> obtenerPorId(@RequestParam(required = true) Integer experienciaId) {
		Experiencia respuesta = service.obtenerPorId(experienciaId);
		if (respuesta != null) {
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/experiencia_registrar")
	public @ResponseBody ResponseEntity<?> registrar(
			@RequestParam(required = true) String empresa,
			@RequestParam(required = true) String inicio, 
			@RequestParam(required = true) String fin,
			@RequestParam(required = true) String cargo, 
			@RequestParam(required = true) String funciones,
			@RequestParam(required = true) String contacto,
			@RequestParam(required = true) String telContacto,
			@RequestParam(required = true) Integer usuarioId) throws ParseException {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Experiencia experiencia = new Experiencia();
		experiencia.setCargo(cargo);
		experiencia.setEmpresa(empresa);
		experiencia.setFunciones(funciones);
		experiencia.setUsuarioId(usuarioId);
		experiencia.setFechaInicio(dateFormat.parse(inicio));
		experiencia.setContacto(contacto);
		experiencia.setTelContacto(telContacto);
		experiencia.setFechaFin(dateFormat.parse(fin));

		Integer respuesta = service.registrar(experiencia);

		if (respuesta != null) {
			return new ResponseEntity<>(new JsonNumber(respuesta), HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/experiencia_eliminar")
	public @ResponseBody ResponseEntity<?> eliminar(
			@RequestParam(required = true) Integer experienciaId){
		
		Experiencia experiencia = service.obtenerPorId(experienciaId);
		service.eliminar(experiencia);
		
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}

}
