package ud.bolsaempleo.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ud.bolsaempleo.EmptyJsonResponse;
import ud.bolsaempleo.model.Aplicacion;
import ud.bolsaempleo.model.Estudio;
import ud.bolsaempleo.model.Experiencia;
import ud.bolsaempleo.model.Oferta;
import ud.bolsaempleo.model.Usuario;
import ud.bolsaempleo.service.AplicacionService;
import ud.bolsaempleo.service.EstudioService;
import ud.bolsaempleo.service.ExperienciaService;
import ud.bolsaempleo.service.OfertaService;
import ud.bolsaempleo.service.UsuarioService;
import ud.bolsaempleo.util.EnvioMail;
import ud.bolsaempleo.util.HojaVidaFile;
import ud.bolsaempleo.util.JsonNumber;

@RestController
@PropertySource(value = { "classpath:application.yml" })
public class AplicacionController {

	@Autowired
	AplicacionService service;

	@Autowired
	UsuarioService usuarioService;
	@Autowired
	OfertaService ofertaService;
	@Autowired
	ExperienciaService experienciaService;
	@Autowired
	EstudioService estudioService;

	@Autowired
	private Environment environment;

	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/aplicacion_registrar")
	public @ResponseBody ResponseEntity<?> registrar(@RequestParam(required = true) Integer ofertaId,
			@RequestParam(required = true) Integer usuarioId) {

		Aplicacion aplicacion = new Aplicacion();
		aplicacion.setFecha(new Date());
		aplicacion.setOfertaId(ofertaId);
		aplicacion.setUsuarioId(usuarioId);

		Integer respuesta = service.registrar(aplicacion);
		if (respuesta != null) {
			return new ResponseEntity<>(new JsonNumber(respuesta), HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/aplicacion_mailAEmpresa")
	public @ResponseBody ResponseEntity<?> enviarMailAEmpresa(@RequestParam(required = true) Integer aspiranteId,
			@RequestParam(required = true) Integer ofertaId) {

		EnvioMail mail = new EnvioMail();
		Usuario aspirante = usuarioService.obtenerPorId(aspiranteId);
		Oferta oferta = ofertaService.obtenerPorId(ofertaId);
		Usuario empresa = usuarioService.obtenerPorId(oferta.getUsuarioId());
		List<Experiencia> experiencias = experienciaService.obtenerPorUsuario(aspiranteId);
		List<Estudio> estudios = estudioService.obtenerPorUsuario(aspiranteId);

		File archivoFinal = null;

		File archivoPDF = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + aspiranteId + ".pdf");
		File archivoDOC = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + aspiranteId + ".doc");
		File archivoDOCX = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + aspiranteId + ".docx");

		if (archivoPDF.exists()) {
			archivoFinal = archivoPDF;
		} else if (archivoDOC.exists()) {
			archivoFinal = archivoDOC;
		} else if (archivoDOCX.exists()) {
			archivoFinal = archivoDOCX;
		}

		mail.mailAplicacionEmpresa(aspirante, experiencias, estudios, oferta, empresa, archivoFinal, environment);
		mail.mailAplicacionOferta(aspirante, oferta, empresa);

		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}

	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping(method = RequestMethod.POST, value = "/aplicacion_subirHV")
	public @ResponseBody ResponseEntity<?> subirHV(@RequestParam(required = true) String nombre,
			@RequestParam(required = true) MultipartFile file) {

		try {
			String aspiranteId = nombre.substring(0,nombre.lastIndexOf("."));
			File archivoPDF = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + aspiranteId + ".pdf");
			File archivoDOC = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + aspiranteId + ".doc");
			File archivoDOCX = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + aspiranteId + ".docx");
			if (archivoPDF.exists()) {
				archivoPDF.delete();
			}
			if (archivoDOC.exists()) {
				archivoDOC.delete();
			}
			if (archivoDOCX.exists()) {
				archivoDOCX.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (!file.isEmpty()) {
			try {
				Files.copy(file.getInputStream(), Paths.get(environment.getRequiredProperty("rutas.adjuntos"), nombre));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);

	}
	
	@RequestMapping(value = "/aplicacion_verificarHV", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<?> descargarDatos(@RequestParam(required = true) Integer usuarioId) {
		try {
			String ruta="",extension="";
			File archivoPDF = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".pdf");
			File archivoDOC = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".doc");
			File archivoDOCX = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".docx");
			if (archivoPDF.exists()) {
				ruta = environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".pdf";
				extension = "pdf";
			}else
			if (archivoDOC.exists()) {
				ruta = environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".doc";
				extension = "doc";
			}else
			if (archivoDOCX.exists()) {
				ruta = environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".docx";
				extension = "docx";
			}
			
			if(!ruta.equals("")){
				return new ResponseEntity<>(new HojaVidaFile("HojaDeVida",extension), HttpStatus.OK);
			}
			
			return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/aplicacion_descargarHV", method = RequestMethod.GET)
	public void descargarDatos(@RequestParam(required = true) String usuarioId,
			HttpServletResponse response) throws Exception {
		try {
			String ruta="",extension="";
			File archivoPDF = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".pdf");
			File archivoDOC = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".doc");
			File archivoDOCX = new File(environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".docx");
			if (archivoPDF.exists()) {
				ruta = environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".pdf";
				extension = "pdf";
			}else
			if (archivoDOC.exists()) {
				ruta = environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".doc";
				extension = "doc";
			}else
			if (archivoDOCX.exists()) {
				ruta = environment.getRequiredProperty("rutas.adjuntos") + "/" + usuarioId + ".docx";
				extension = "docx";
			}
			
			if(!ruta.equals("")){
				generarDescarga(response, ruta, "HojaDeVida."+extension, extension);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void generarDescarga(HttpServletResponse response, String rutaArchivo, String nombreArchivo, String tipo) {
		ServletOutputStream stream = null;
		FileInputStream input=null;
		BufferedInputStream buf=null;
		try {
			response.setContentType("application/"+tipo);
			response.setHeader("Cache-Control", "no-cache"); // HTTP 1.1
			response.setHeader("Cache-Control", "max-age=0");
			response.setHeader("Content-disposition", "attachment; filename="
					+ nombreArchivo);
			stream = response.getOutputStream();
			input = new FileInputStream(rutaArchivo);
			buf = new BufferedInputStream(input);
			int readBytes = 0;

			while ((readBytes = buf.read()) != -1) {
				stream.write(readBytes);
			}
			stream.flush();
			buf.close();
			stream.close();
		} catch (Exception e) {
			try {
				buf.close();
				stream.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
	

}
