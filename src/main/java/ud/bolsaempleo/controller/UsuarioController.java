package ud.bolsaempleo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ud.bolsaempleo.EmptyJsonResponse;
import ud.bolsaempleo.model.Usuario;
import ud.bolsaempleo.service.UsuarioService;
import ud.bolsaempleo.util.JsonNumber;

@RestController
public class UsuarioController {
	
	@Autowired
	UsuarioService service;
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/usuario_obtenerPorId")
	public @ResponseBody ResponseEntity<?> obtenerPorId(
			@RequestParam(required = true) Integer usuarioId) {
		Usuario respuesta = service.obtenerPorId(usuarioId);
		if (respuesta != null) {
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/usuario_obtenerPorEmailPass")
	public @ResponseBody ResponseEntity<?> obtenerPorIdPass(
			@RequestParam(required = true) String email,
			@RequestParam(required = true) String password) {
		Usuario respuesta = service.obtenerPorEmailPass(email, password);
		if (respuesta != null) {
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping("/usuario_recuperarPassword")
	public @ResponseBody ResponseEntity<?> recuperarPassword(
			@RequestParam(required = true) String email) {
		
		service.recuperarPassword(email);
		
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping(value = "/usuario_registrar")
	public @ResponseBody ResponseEntity<?> registrar(
			@RequestParam(required = true) String rol,
			@RequestParam(required = true) String nombre,
			@RequestParam(required = true) String email,
			@RequestParam(required = true) String password,
			//@RequestParam(required = true) String inputPasswordConf,
			@RequestParam(required = true) String tipoDocumento,
			@RequestParam(required = true) String documento,
			@RequestParam(required = true) String telefono,
			@RequestParam(required = true) String perfil,
			@RequestParam(required = true) String pais,
			@RequestParam(required = true) String ciudad,
			@RequestParam(required = false) String libreta,
			@RequestParam(required = false) String direccion,
			@RequestParam(required = false) String paginaWeb,
			@RequestParam(required = false) String genero,
			@RequestParam(required = false) String estadoCivil,
			@RequestParam(required = false) String licencia,
			@RequestParam(required = false) String catLicencia
			) {
		
		Usuario usuario = new Usuario();
		usuario.setRol(rol);
		usuario.setNombre(nombre);
		usuario.setEmail(email);
		usuario.setPass(password);
		usuario.setTipoDoc(tipoDocumento);
		usuario.setDoc(documento);
		usuario.setTelefono(telefono);
		usuario.setPerfil(perfil);
		usuario.setPais(pais);
		usuario.setCiudad(ciudad);
		usuario.setLibreta(libreta);
		usuario.setDireccion(direccion);
		usuario.setWeb(paginaWeb);
		usuario.setGenero(genero);
		usuario.setEstadoCivil(estadoCivil);
		usuario.setLicenciaCond(licencia);
		usuario.setCatLicenciaCond(catLicencia);

		Integer respuesta = service.registrar(usuario);
		
		if (respuesta != null) {
			return new ResponseEntity<>(new JsonNumber(respuesta), HttpStatus.OK);
		}
		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.NOT_FOUND);
	}
	
	
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping(value = "/usuario_editar")
	public @ResponseBody ResponseEntity<?> editar(
			@RequestParam(required = true) Integer usuarioId,
			@RequestParam(required = false) String nombre,
			@RequestParam(required = false) String password,
			@RequestParam(required = false) String tipoDocumento,
			@RequestParam(required = false) String documento,
			@RequestParam(required = false) String telefono,
			@RequestParam(required = false) String perfil,
			@RequestParam(required = false) String pais,
			@RequestParam(required = false) String ciudad,
			@RequestParam(required = false) String libreta,
			@RequestParam(required = false) String direccion,
			@RequestParam(required = false) String paginaWeb,
			@RequestParam(required = false) String genero,
			@RequestParam(required = false) String estadoCivil,
			@RequestParam(required = false) String licencia,
			@RequestParam(required = false) String catLicencia
			) {
		
		Usuario usuario = new Usuario();
		usuario.setId(usuarioId);
		usuario.setNombre(nombre);
		usuario.setPass(password);
		usuario.setTipoDoc(tipoDocumento);
		usuario.setDoc(documento);
		usuario.setTelefono(telefono);
		usuario.setPerfil(perfil);
		usuario.setPais(pais);
		usuario.setCiudad(ciudad);
		usuario.setLibreta(libreta);
		usuario.setDireccion(direccion);
		usuario.setWeb(paginaWeb);
		usuario.setGenero(genero);
		usuario.setEstadoCivil(estadoCivil);
		usuario.setLicenciaCond(licencia);
		usuario.setCatLicenciaCond(catLicencia);

		service.editar(usuario);

		return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
	}

}
