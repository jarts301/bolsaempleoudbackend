package ud.bolsaempleo.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import ud.bolsaempleo.dao.SectorDao;
import ud.bolsaempleo.model.Sector;

@Repository("SectorDao")
public class SectorDaoImpl extends AbstractDao<Integer, Sector> implements SectorDao {

	@Override
	public Sector obtenerPorId(Integer sectorId) {
		return getByKey(sectorId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Sector> obtenerTodos() {
		Session session = getSession();
		Query query = session.getNamedQuery("sector_obtenerTodos");
		return (List<Sector>) query.list();
	}

}
