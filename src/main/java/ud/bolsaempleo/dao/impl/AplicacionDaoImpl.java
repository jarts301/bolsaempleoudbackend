package ud.bolsaempleo.dao.impl;

import org.springframework.stereotype.Repository;

import ud.bolsaempleo.dao.AplicacionDao;
import ud.bolsaempleo.model.Aplicacion;

@Repository("AplicacionDao")
public class AplicacionDaoImpl extends AbstractDao<Integer, Aplicacion> implements AplicacionDao {

	@Override
	public Integer registrar(Aplicacion aplicacion) {
		persist(aplicacion);
		return aplicacion.getId();
	}

}
