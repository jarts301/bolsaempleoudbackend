package ud.bolsaempleo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import ud.bolsaempleo.dao.OfertaDao;
import ud.bolsaempleo.model.Oferta;

@Repository("OfertaDao")
public class OfertaDaoImpl extends AbstractDao<Integer, Oferta> implements OfertaDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Oferta> obtenerPorSector(Integer sectorId) {
		Session session = getSession();
		Query query = session.getNamedQuery("oferta_obtenerPorSector").setParameter("sectorId", sectorId);
		return (List<Oferta>) query.list();
	}

	@Override
	public Oferta obtenerPorId(Integer ofertaId) {
		return getByKey(ofertaId);
	}

	@Override
	public Integer registrar(Oferta oferta) {
		persist(oferta);
		return oferta.getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Oferta> actualizarActivo() {
		Session session = getSession();
		Query query = session.getNamedQuery("oferta_actualizarActivo");
		return (ArrayList<Oferta>) query.list();
	}
}
