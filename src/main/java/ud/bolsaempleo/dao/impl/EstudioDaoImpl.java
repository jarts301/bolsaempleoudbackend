package ud.bolsaempleo.dao.impl;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import ud.bolsaempleo.dao.EstudioDao;
import ud.bolsaempleo.model.Estudio;

@Repository("EstudioDao")
public class EstudioDaoImpl extends AbstractDao<Integer, Estudio> implements EstudioDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Estudio> obtenerPorUsuario(Integer usuarioId) {
		Session session = getSession();
		Query query = session.getNamedQuery("estudio_obtenerPorUsuario").setParameter("usuarioId", usuarioId);
		return (List<Estudio>) query.list();
	}

	@Override
	public Estudio obtenerPorId(Integer estudioId) {
		return getByKey(estudioId);
	}

	@Override
	public Integer registrar(Estudio estudio) {
		persist(estudio);
		return estudio.getId();
	}

	@Override
	public void eliminar(Estudio estudio) {
		delete(estudio);
	}
	
}
