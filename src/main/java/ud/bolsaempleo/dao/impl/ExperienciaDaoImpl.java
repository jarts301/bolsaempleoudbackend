package ud.bolsaempleo.dao.impl;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import ud.bolsaempleo.dao.ExperienciaDao;
import ud.bolsaempleo.model.Experiencia;

@Repository("ExperienciaDao")
public class ExperienciaDaoImpl extends AbstractDao<Integer, Experiencia> implements ExperienciaDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Experiencia> obtenerPorUsuario(Integer usuarioId) {
		Session session = getSession();
		Query query = session.getNamedQuery("experiencia_obtenerPorUsuario").setParameter("usuarioId", usuarioId);
		return (List<Experiencia>) query.list();
	}

	@Override
	public Experiencia obtenerPorId(Integer experienciaId) {
		return getByKey(experienciaId);
	}

	@Override
	public Integer registrar(Experiencia experiencia) {
		persist(experiencia);
		return experiencia.getId();
	}

	@Override
	public void eliminar(Experiencia experiencia) {
		delete(experiencia);
	}
	
}
