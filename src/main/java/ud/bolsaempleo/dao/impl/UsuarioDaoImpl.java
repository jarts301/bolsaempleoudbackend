package ud.bolsaempleo.dao.impl;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import ud.bolsaempleo.dao.UsuarioDao;
import ud.bolsaempleo.model.Usuario;

@Repository("UsuarioDao")
public class UsuarioDaoImpl extends AbstractDao<Integer, Usuario> implements UsuarioDao {

	@Override
	public Usuario obtenerPorId(Integer usuarioId) {
		return getByKey(usuarioId);
	}

	@Override
	public Integer registrar(Usuario usuario) {
		persist(usuario);
		return usuario.getId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Usuario obtenerPorEmailPass(String email, String pass) {
		Session session = getSession();
		Query query = session.getNamedQuery("usuario_obtenerPorEmailPass").setParameter("email", email)
				.setParameter("pass", pass);
		if (query.list().size() > 0) {
			return ((ArrayList<Usuario>) query.list()).get(0);
		} else {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Usuario obtenerPorEmail(String usuarioEmail) {
		Session session = getSession();
		Query query = session.getNamedQuery("usuario_obtenerPorEmail").setParameter("email", usuarioEmail);
		if (query.list().size() > 0) {
			return ((ArrayList<Usuario>) query.list()).get(0);
		} else {
			return null;
		}
	}

	@Override
	public void editar(Usuario usuario) {
		merge(usuario);
	}

}
