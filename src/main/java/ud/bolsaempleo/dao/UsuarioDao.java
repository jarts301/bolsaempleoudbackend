package ud.bolsaempleo.dao;

import ud.bolsaempleo.model.Usuario;

public interface UsuarioDao {
	
	public Usuario obtenerPorId(Integer usuarioId);
	
	public Usuario obtenerPorEmailPass(String email, String pass);
	
	public Usuario obtenerPorEmail(String usuarioEmail);
	
	public Integer registrar(Usuario usuario);
	
	public void editar(Usuario usuario);

}
