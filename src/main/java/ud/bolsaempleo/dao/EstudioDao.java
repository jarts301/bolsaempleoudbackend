package ud.bolsaempleo.dao;

import java.util.List;

import ud.bolsaempleo.model.Estudio;

public interface EstudioDao {

	public List<Estudio> obtenerPorUsuario(Integer usuarioId);
	
	public Estudio obtenerPorId(Integer estudioId);
	
	public Integer registrar(Estudio estudio);
	
	public void eliminar(Estudio estudio);

}
