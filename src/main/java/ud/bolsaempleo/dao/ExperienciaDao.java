package ud.bolsaempleo.dao;

import java.util.List;

import ud.bolsaempleo.model.Experiencia;

public interface ExperienciaDao {

	public List<Experiencia> obtenerPorUsuario(Integer usuarioId);
	
	public Experiencia obtenerPorId(Integer experienciaId);
	
	public Integer registrar(Experiencia experiencia);
	
	public void eliminar(Experiencia experiencia);

}
