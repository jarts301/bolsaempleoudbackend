package ud.bolsaempleo.dao;

import java.util.ArrayList;
import java.util.List;

import ud.bolsaempleo.model.Oferta;

public interface OfertaDao {
	
	public List<Oferta> obtenerPorSector(Integer sectorId);
	
	public Oferta obtenerPorId(Integer ofertaId);
	
	public Integer registrar(Oferta oferta);
	
	public ArrayList<Oferta> actualizarActivo();

}
