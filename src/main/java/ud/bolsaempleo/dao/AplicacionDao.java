package ud.bolsaempleo.dao;

import ud.bolsaempleo.model.Aplicacion;

public interface AplicacionDao {
	
	public Integer registrar(Aplicacion aplicacion);

}
