package ud.bolsaempleo.dao;

import java.util.List;

import ud.bolsaempleo.model.Sector;

public interface SectorDao {
	
	public Sector obtenerPorId(Integer sectorId);
	
	public List<Sector> obtenerTodos();

}
