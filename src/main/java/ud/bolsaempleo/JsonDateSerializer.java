package ud.bolsaempleo;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonSerializer;

@Component
public class JsonDateSerializer extends JsonSerializer<Date> {
	
	@Override
	public void serialize(Date date, com.fasterxml.jackson.core.JsonGenerator gen,
			com.fasterxml.jackson.databind.SerializerProvider provider)
			throws IOException, com.fasterxml.jackson.core.JsonProcessingException {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String formattedDate = dateFormat.format(date);
		gen.writeString(formattedDate);
		
	}
}
