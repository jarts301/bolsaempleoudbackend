package ud.bolsaempleo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ud.bolsaempleo.JsonDateSerializer;

@NamedNativeQueries({
	@NamedNativeQuery(
	name = "oferta_obtenerPorSector",
	query = "CALL oferta_obtenerPorSector(:sectorId)",
	resultClass = Oferta.class
	),
	@NamedNativeQuery(
	name = "oferta_actualizarActivo",
	query = "CALL oferta_actualizarActivo()",
	resultClass = Oferta.class
	)
})

@Entity
@Table(name="oferta")
public class Oferta {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "oferta_id", nullable = true)
    private int id;
    
    @Column(name = "oferta_cargo", nullable = false)
    private String cargo;
    
    @Column(name = "oferta_salario", nullable = false)
    private String salario;
    
    @Column(name = "oferta_descripcion", nullable = false)
    private String descripcion;
    
    @Column(name = "oferta_fecha_inicial", nullable = false)
    private Date fechaInicial;
    
    @Column(name = "oferta_fecha_final", nullable = false)
    private Date fechaFinal;

	@Column(name = "oferta_activa", nullable = false)
    private boolean activa;
	
	@Column(name = "oferta_pais", nullable = false)
    private String pais;
	
	@Column(name = "oferta_ciudad", nullable = false)
    private String ciudad;

	@Column(name = "sector_id", nullable = false)
    private int sectorId;
    
    @Column(name = "usuario_id", nullable = false)
    private int usuarioId;
    
    /*@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "sector_id", nullable = false)
    private Sector sector;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;*/
    
    /*@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="id")
    private List<Aplicacion> aplicaciones;*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getSalario() {
		return salario;
	}

	public void setSalario(String salario) {
		this.salario = salario;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@JsonSerialize(using=JsonDateSerializer.class)
    public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public boolean isActiva() {
		return activa;
	}

	public void setActiva(boolean activa) {
		this.activa = activa;
	}
	
    public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public int getSectorId() {
		return sectorId;
	}

	public void setSectorId(int sectorId) {
		this.sectorId = sectorId;
	}

	public int getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}

}
