package ud.bolsaempleo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
	name = "sector_obtenerTodos",
	query = "CALL sector_obtenerTodos()",
	resultClass = Sector.class
	)
})

@Entity
@Table(name="sector")
public class Sector {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "sector_id", nullable = true)
    private int id;
    
    @Column(name = "sector_codigo", nullable = false)
    private String codigo;
    
    @Column(name = "sector_nombre", nullable = false)
    private String nombre;
    
    /*@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="id")
    private List<Oferta> ofertas;*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
