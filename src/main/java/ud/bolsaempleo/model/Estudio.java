package ud.bolsaempleo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ud.bolsaempleo.JsonDateSerializer;

@NamedNativeQueries({
	@NamedNativeQuery(
	name = "estudio_obtenerPorUsuario",
	query = "CALL estudio_obtenerPorUsuario(:usuarioId)",
	resultClass = Estudio.class
	)
})

@Entity
@Table(name="estudio")
public class Estudio {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "estudio_id", nullable = true)
    private int id;
    
    @Column(name = "estudio_nombre", nullable = true)
    private String nombre;
    
    @Column(name = "estudio_inicio", nullable = true)
    private Date inicio;
    
    @Column(name = "estudio_terminacion", nullable = true)
    private Date terminacion;
    
    @Column(name = "estudio_tipo", nullable = true)
    private String tipo;
    
    @Column(name = "estudio_institucion", nullable = true)
    private String institucion;
    
    @Column(name = "usuario_id", nullable = false)
    private int usuarioId;
    
    /*@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getTerminacion() {
		return terminacion;
	}

	public void setTerminacion(Date terminacion) {
		this.terminacion = terminacion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getInstitucion() {
		return institucion;
	}

	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}

	public int getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}
}
