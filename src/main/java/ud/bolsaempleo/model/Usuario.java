package ud.bolsaempleo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

@NamedNativeQueries({
	@NamedNativeQuery(
	name = "usuario_obtenerPorEmailPass",
	query = "CALL usuario_obtenerPorEmailPass(:email,:pass)",
	resultClass = Usuario.class
	),
	@NamedNativeQuery(
	name = "usuario_obtenerPorEmail",
	query = "SELECT u.* FROM usuario u WHERE u.usuario_email = :email ;",
	resultClass = Usuario.class
	)
})

@Entity
@Table(name="usuario")
public class Usuario {
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "usuario_id", nullable = true)
    private int id;
    
    @Column(name = "usuario_nombre", nullable = false)
    private String nombre;
    
    @Column(name = "usuario_email", nullable = false)
    private String email;
    
    @Column(name = "usuario_pass", nullable = false)
    private String pass;
    
    @Column(name = "usuario_rol", nullable = false)
    private String rol;
    
    @Column(name = "usuario_doc", nullable = false)
    private String doc;
    
    @Column(name = "usuario_tipodoc", nullable = false)
    private String tipoDoc;
    
    @Column(name = "usuario_telefono", nullable = false)
    private String telefono;
    
    @Column(name = "usuario_perfil", nullable = false)
    private String perfil;
    
    @Column(name = "usuario_libreta", nullable = true)
    private String libreta;
    
    @Column(name = "usuario_direccion", nullable = true)
    private String direccion;
    
    @Column(name = "usuario_web", nullable = true)
    private String web;
    
    @Column(name = "usuario_pais", nullable = false)
    private String pais;
    
    @Column(name = "usuario_ciudad", nullable = false)
    private String ciudad;
    
    @Column(name = "usuario_genero", nullable = true)
    private String genero;
    
    @Column(name = "usuario_estadocivil", nullable = true)
    private String estadoCivil;
    
    @Column(name = "usuario_licenciacond", nullable = true)
    private String licenciaCond;
    
    @Column(name = "usuario_catlicenciacond", nullable = true)
    private String catLicenciaCond;
    
    /*@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="id")
    private List<Oferta> ofertas;
    
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="id")
    private List<Aplicacion> aplicaciones;
    
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="id")
    private List<Estudio> estudios;
    
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="id")
    private List<Experiencia> experiencias;*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getDoc() {
		return doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getLibreta() {
		return libreta;
	}

	public void setLibreta(String libreta) {
		this.libreta = libreta;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getWeb() {
		return web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getLicenciaCond() {
		return licenciaCond;
	}

	public void setLicenciaCond(String licenciaCond) {
		this.licenciaCond = licenciaCond;
	}

	public String getCatLicenciaCond() {
		return catLicenciaCond;
	}

	public void setCatLicenciaCond(String catLicenciaCond) {
		this.catLicenciaCond = catLicenciaCond;
	}
    
}
