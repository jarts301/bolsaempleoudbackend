package ud.bolsaempleo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ud.bolsaempleo.JsonDateSerializer;

@NamedNativeQueries({
	@NamedNativeQuery(
	name = "experiencia_obtenerPorUsuario",
	query = "CALL experiencia_obtenerPorUsuario(:usuarioId)",
	resultClass = Experiencia.class
	)
})

@Entity
@Table(name="experiencia")
public class Experiencia {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "experiencia_id", nullable = true)
    private int id;
    
    @Column(name = "experiencia_empresa", nullable = true)
    private String empresa;
    
    @Column(name = "experiencia_fechainicio", nullable = true)
    private Date fechaInicio;
    
    @Column(name = "experiencia_fechafin", nullable = true)
    private Date fechaFin;
    
    @Column(name = "experiencia_cargo", nullable = true)
    private String cargo;
    
    @Column(name = "experiencia_funciones", nullable = true)
    private String funciones;
    
    @Column(name = "experiencia_contacto", nullable = true)
    private String contacto;
    
    @Column(name = "experiencia_tel_contacto", nullable = true)
    private String telContacto;
    
    @Column(name = "usuario_id", nullable = false)
    private int usuarioId;
    
    /*@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getFunciones() {
		return funciones;
	}

	public void setFunciones(String funciones) {
		this.funciones = funciones;
	}

	public int getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}
	
	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getTelContacto() {
		return telContacto;
	}

	public void setTelContacto(String telContacto) {
		this.telContacto = telContacto;
	}
    
}
