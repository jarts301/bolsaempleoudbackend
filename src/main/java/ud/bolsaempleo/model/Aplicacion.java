package ud.bolsaempleo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ud.bolsaempleo.JsonDateSerializer;

@Entity
@Table(name="aplicacion")
public class Aplicacion {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "aplicacion_id", nullable = true)
    private int id;
    
    @Column(name = "aplicacion_fecha", nullable = false)
    private Date fecha;
    
    @Column(name = "usuario_id", nullable = false)
    private int usuarioId;
    
    @Column(name = "oferta_id", nullable = false)
    private int ofertaId;
    
    /*@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;*/
    
    /*@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "oferta_id", nullable = false)
    private Oferta oferta;*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}

	public int getOfertaId() {
		return ofertaId;
	}

	public void setOfertaId(int ofertaId) {
		this.ofertaId = ofertaId;
	}
	
	
}
