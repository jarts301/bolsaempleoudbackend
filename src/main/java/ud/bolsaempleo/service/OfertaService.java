package ud.bolsaempleo.service;

import java.util.List;

import ud.bolsaempleo.model.Oferta;

public interface OfertaService {
	
	public List<Oferta> obtenerPorSector(Integer sectorId);
	
	public Oferta obtenerPorId(Integer ofertaId);
	
	public Integer registrar(Oferta oferta);
	
	public void actualizarActivo();

}
