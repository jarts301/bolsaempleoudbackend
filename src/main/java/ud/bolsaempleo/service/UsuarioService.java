package ud.bolsaempleo.service;

import ud.bolsaempleo.model.Usuario;

public interface UsuarioService {
	
	public Usuario obtenerPorId(Integer usuarioId);
	
	public Usuario obtenerPorEmailPass(String email,String pass);
	
	public Integer registrar(Usuario usuario);
	
	public void editar(Usuario usuario);
	
	public void recuperarPassword(String usuarioEmail);

}
