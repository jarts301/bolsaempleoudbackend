package ud.bolsaempleo.service;

import java.util.List;

import ud.bolsaempleo.model.Estudio;

public interface EstudioService {
	
	public List<Estudio> obtenerPorUsuario(Integer usuarioId);
	
	public Estudio obtenerPorId(Integer estudioId);
	
	public Integer registrar(Estudio estudio);
	
	public void eliminar(Estudio estudio);

}
