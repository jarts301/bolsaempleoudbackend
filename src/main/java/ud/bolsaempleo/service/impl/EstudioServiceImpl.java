package ud.bolsaempleo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ud.bolsaempleo.dao.EstudioDao;
import ud.bolsaempleo.model.Estudio;
import ud.bolsaempleo.service.EstudioService;

@Service("EstudioService")
@Transactional
public class EstudioServiceImpl implements EstudioService {

	@Autowired
	EstudioDao estudioDao;

	@Override
	public List<Estudio> obtenerPorUsuario(Integer usuarioId) {
		return estudioDao.obtenerPorUsuario(usuarioId);
	}

	@Override
	public Estudio obtenerPorId(Integer estudioId) {
		return estudioDao.obtenerPorId(estudioId);
	}

	@Override
	public Integer registrar(Estudio estudio) {
		return estudioDao.registrar(estudio);
	}

	@Override
	public void eliminar(Estudio estudio) {
		estudioDao.eliminar(estudio);
	}

}
