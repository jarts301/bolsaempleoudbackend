package ud.bolsaempleo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ud.bolsaempleo.dao.AplicacionDao;
import ud.bolsaempleo.model.Aplicacion;
import ud.bolsaempleo.service.AplicacionService;

@Service("AplicacionService")
@Transactional
public class AplicacionServiceImpl implements AplicacionService {

	@Autowired
	AplicacionDao aplicacionDao;

	@Override
	public Integer registrar(Aplicacion aplicacion) {
		return aplicacionDao.registrar(aplicacion);
	}

}
