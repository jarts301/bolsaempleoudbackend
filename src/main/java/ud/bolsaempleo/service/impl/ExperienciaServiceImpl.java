package ud.bolsaempleo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ud.bolsaempleo.dao.ExperienciaDao;
import ud.bolsaempleo.model.Experiencia;
import ud.bolsaempleo.service.ExperienciaService;

@Service("ExperienciaService")
@Transactional
public class ExperienciaServiceImpl implements ExperienciaService {

	@Autowired
	ExperienciaDao experienciaDao;

	@Override
	public List<Experiencia> obtenerPorUsuario(Integer usuarioId) {
		return experienciaDao.obtenerPorUsuario(usuarioId);
	}

	@Override
	public Experiencia obtenerPorId(Integer experienciaId) {
		return experienciaDao.obtenerPorId(experienciaId);
	}

	@Override
	public Integer registrar(Experiencia experiencia) {
		return experienciaDao.registrar(experiencia);
	}

	@Override
	public void eliminar(Experiencia experiencia) {
		experienciaDao.eliminar(experiencia);
	}

}
