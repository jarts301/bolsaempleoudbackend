package ud.bolsaempleo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ud.bolsaempleo.dao.OfertaDao;
import ud.bolsaempleo.model.Oferta;
import ud.bolsaempleo.service.OfertaService;

@Service("OfertaService")
@Transactional
@EnableScheduling
public class OfertaServiceImpl implements OfertaService {

	@Autowired
	OfertaDao ofertaDao;

	@Override
	public List<Oferta> obtenerPorSector(Integer sectorId) {
		return ofertaDao.obtenerPorSector(sectorId);
	}

	@Override
	public Oferta obtenerPorId(Integer ofertaId) {
		return ofertaDao.obtenerPorId(ofertaId);
	}

	@Override
	public Integer registrar(Oferta oferta) {
		return ofertaDao.registrar(oferta);
	}

	@Override
	@Scheduled(cron = "0 0 1 * * *") //second, minute, hour, day, month, weekday, (* All)
	public void actualizarActivo() {
		System.out.println("Ofertas actualizadas correctamente");
		ofertaDao.actualizarActivo();
	}

}
