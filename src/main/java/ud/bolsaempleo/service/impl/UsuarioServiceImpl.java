package ud.bolsaempleo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ud.bolsaempleo.dao.UsuarioDao;
import ud.bolsaempleo.model.Usuario;
import ud.bolsaempleo.service.UsuarioService;
import ud.bolsaempleo.util.EnvioMail;
import ud.bolsaempleo.util.UsuarioUtil;

@Service("UsuarioService")
@Transactional
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioDao usuarioDao;

	@Override
	public Usuario obtenerPorId(Integer usuarioId) {
		return usuarioDao.obtenerPorId(usuarioId);
	}

	@Override
	public Integer registrar(Usuario usuario) {
		return usuarioDao.registrar(usuario);
	}

	@Override
	public Usuario obtenerPorEmailPass(String email, String pass) {
		return usuarioDao.obtenerPorEmailPass(email, pass);
	}

	@Override
	public void editar(Usuario usuario) {
		Usuario registro = usuarioDao.obtenerPorId(usuario.getId());

		if (usuario.getCatLicenciaCond() != null) {
			registro.setCatLicenciaCond(usuario.getCatLicenciaCond());
		}
		if (usuario.getCiudad() != null) {
			registro.setCiudad(usuario.getCiudad());
		}
		if (usuario.getDireccion() != null) {
			registro.setDireccion(usuario.getDireccion());
		}
		if (usuario.getDoc() != null) {
			registro.setDoc(usuario.getDoc());
		}
		if (usuario.getEmail() != null) {
			registro.setEmail(usuario.getEmail());
		}
		if (usuario.getEstadoCivil() != null) {
			registro.setEstadoCivil(usuario.getEstadoCivil());
		}
		if (usuario.getGenero() != null) {
			registro.setGenero(usuario.getGenero());
		}
		if (usuario.getLibreta() != null) {
			registro.setLibreta(usuario.getLibreta());
		}
		if (usuario.getLicenciaCond() != null) {
			registro.setLicenciaCond(usuario.getLicenciaCond());
		}
		if (usuario.getNombre() != null) {
			registro.setNombre(usuario.getNombre());
		}
		if (usuario.getPais() != null) {
			registro.setPais(usuario.getPais());
		}
		if (usuario.getPass() != null) {
			registro.setPass(usuario.getPass());
		}
		if (usuario.getPerfil() != null) {
			registro.setPerfil(usuario.getPerfil());
		}
		if (usuario.getRol() != null) {
			registro.setRol(usuario.getRol());
		}
		if (usuario.getTelefono() != null) {
			registro.setTelefono(usuario.getTelefono());
		}
		if (usuario.getTipoDoc() != null) {
			registro.setTipoDoc(usuario.getTipoDoc());
		}
		if (usuario.getWeb() != null) {
			registro.setWeb(usuario.getWeb());
		}

		usuarioDao.editar(registro);

	}

	@Override
	public void recuperarPassword(String usuarioEmail) {
		Usuario usuario = usuarioDao.obtenerPorEmail(usuarioEmail);
		String nuevaPassword = (new UsuarioUtil()).getCadenaAlfanumAleatoria(8);
		usuario.setPass(nuevaPassword);
		usuarioDao.editar(usuario);
		EnvioMail mail = new EnvioMail();
		mail.mailRecuperarPassword(usuario);
	}

}
