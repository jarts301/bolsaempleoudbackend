package ud.bolsaempleo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ud.bolsaempleo.dao.SectorDao;
import ud.bolsaempleo.model.Sector;
import ud.bolsaempleo.service.SectorService;

@Service("SectorService")
@Transactional
public class SectorServiceImpl implements SectorService {

	@Autowired
	SectorDao sectorDao;

	@Override
	public Sector obtenerPorId(Integer sectorId) {
		return sectorDao.obtenerPorId(sectorId);
	}

	@Override
	public List<Sector> obtenerTodos() {
		return sectorDao.obtenerTodos();
	}

}
