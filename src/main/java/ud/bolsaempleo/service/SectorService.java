package ud.bolsaempleo.service;

import java.util.List;

import ud.bolsaempleo.model.Sector;

public interface SectorService {
	
	public Sector obtenerPorId(Integer sectorId);
	
	public List<Sector> obtenerTodos();

}
