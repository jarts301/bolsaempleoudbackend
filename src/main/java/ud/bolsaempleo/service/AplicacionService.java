package ud.bolsaempleo.service;

import ud.bolsaempleo.model.Aplicacion;

public interface AplicacionService {
	
	public Integer registrar(Aplicacion aplicacion);

}
