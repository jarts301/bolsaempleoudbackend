package ud.bolsaempleo.service;

import java.util.List;

import ud.bolsaempleo.model.Experiencia;

public interface ExperienciaService {
	
	public List<Experiencia> obtenerPorUsuario(Integer usuarioId);
	
	public Experiencia obtenerPorId(Integer experienciaId);
	
	public Integer registrar(Experiencia experiencia);
	
	public void eliminar(Experiencia experiencia);

}
