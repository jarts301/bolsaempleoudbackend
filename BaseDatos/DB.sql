-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: bolsa_empleo_ud
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aplicacion`
--

DROP TABLE IF EXISTS `aplicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aplicacion` (
  `aplicacion_id` int(11) NOT NULL,
  `aplicacion_fecha` datetime NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `oferta_id` int(11) NOT NULL,
  PRIMARY KEY (`aplicacion_id`),
  KEY `fk_Usuario_has_Oferta_Oferta1_idx` (`oferta_id`),
  KEY `fk_Usuario_has_Oferta_Usuario1_idx` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aplicacion`
--

LOCK TABLES `aplicacion` WRITE;
/*!40000 ALTER TABLE `aplicacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `aplicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estudio`
--

DROP TABLE IF EXISTS `estudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estudio` (
  `estudio_id` int(11) NOT NULL AUTO_INCREMENT,
  `estudio_nombre` varchar(50) DEFAULT NULL COMMENT 'Profesional, bachiller etc...',
  `estudio_inicio` date DEFAULT NULL,
  `estudio_terminacion` date DEFAULT NULL,
  `estudio_tipo` varchar(200) DEFAULT NULL,
  `estudio_institucion` varchar(200) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`estudio_id`),
  KEY `fk_Estudio_Usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_Estudio_Usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estudio`
--

LOCK TABLES `estudio` WRITE;
/*!40000 ALTER TABLE `estudio` DISABLE KEYS */;
INSERT INTO `estudio` VALUES (1,'Bachiller Academico','1999-12-01','2010-12-01','Bachiller','Colegio Santa Clara',1);
/*!40000 ALTER TABLE `estudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experiencia`
--

DROP TABLE IF EXISTS `experiencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiencia` (
  `experiencia_id` int(11) NOT NULL,
  `experiencia_empresa` varchar(200) DEFAULT NULL,
  `experiencia_fechainicio` date DEFAULT NULL,
  `experiencia_fechafin` date DEFAULT NULL,
  `experiencia_cargo` varchar(200) DEFAULT NULL,
  `experiencia_funciones` varchar(4000) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`experiencia_id`),
  KEY `fk_Experiencia_Usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_Experiencia_Usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experiencia`
--

LOCK TABLES `experiencia` WRITE;
/*!40000 ALTER TABLE `experiencia` DISABLE KEYS */;
INSERT INTO `experiencia` VALUES (1,'Tecnology World','2011-02-01','2015-02-01','Vendedor','Vendedor de todo tipo de articulos electronicos para el hogar.',1);
/*!40000 ALTER TABLE `experiencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oferta`
--

DROP TABLE IF EXISTS `oferta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oferta` (
  `oferta_id` int(11) NOT NULL,
  `oferta_cargo` varchar(200) NOT NULL,
  `oferta_salario` varchar(200) NOT NULL,
  `oferta_descripcion` varchar(4000) NOT NULL,
  `oferta_fecha` datetime NOT NULL,
  `sector_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`oferta_id`),
  KEY `fk_Oferta_Sector_idx` (`sector_id`),
  KEY `fk_Oferta_Usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_Oferta_Sector` FOREIGN KEY (`sector_id`) REFERENCES `sector` (`sector_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Oferta_Usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oferta`
--

LOCK TABLES `oferta` WRITE;
/*!40000 ALTER TABLE `oferta` DISABLE KEYS */;
INSERT INTO `oferta` VALUES (1,'Vendedor Tecnologia','1200000','Encargado de la sede de ventas N5, toda la gestión de esta. con 3 o mas años de experiencia en el area. ','2016-11-10 00:00:00',1,2);
/*!40000 ALTER TABLE `oferta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sector`
--

DROP TABLE IF EXISTS `sector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sector` (
  `sector_id` int(11) NOT NULL,
  `sector_codigo` varchar(45) NOT NULL,
  `sector_nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`sector_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sector`
--

LOCK TABLES `sector` WRITE;
/*!40000 ALTER TABLE `sector` DISABLE KEYS */;
INSERT INTO `sector` VALUES (1,'V','Ventas'),(2,'POM','Producción / Operarios / Manufactura'),(3,'A','Atención a clientes'),(4,'AO','Administración / Oficina'),(5,'ALT','Almacén / Logística / Transporte'),(6,'MS','Medicina / Salud'),(7,'SGAS','Servicios Generales, Aseo y Seguridad'),(8,'CO','Construccion y obra'),(9,'CF','Contabilidad / Finanzas'),(10,'CT','CallCenter / Telemercadeo'),(11,'MRT','Mantenimiento y Reparaciones Técnicas'),(12,'IT','Informática / Telecomunicaciones'),(13,'I','Ingeniería'),(14,'HT','Hostelería / Turismo'),(15,'RH','Recursos Humanos'),(16,'MPC','Mercadotécnia / Publicidad / Comunicación'),(17,'D','Docencia'),(18,'DA','Diseño / Artes gráficas'),(19,'IC','Investigación y Calidad'),(20,'CC','Compras / Comercio Exterior'),(21,'LA','Legal / Asesoría'),(22,'DG','Dirección / Gerencia');
/*!40000 ALTER TABLE `sector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_nombre` varchar(200) NOT NULL,
  `usuario_email` varchar(200) NOT NULL,
  `usuario_pass` varchar(2000) NOT NULL,
  `usuario_rol` varchar(50) NOT NULL COMMENT 'Empresa\nAspirante',
  `usuario_doc` varchar(50) NOT NULL,
  `usuario_tipodoc` varchar(50) NOT NULL,
  `usuario_telefono` varchar(100) NOT NULL,
  `usuario_perfil` varchar(4000) NOT NULL,
  `usuario_pais` varchar(60) DEFAULT NULL,
  `usuario_ciudad` varchar(60) DEFAULT NULL,
  `usuario_genero` varchar(45) DEFAULT NULL,
  `usuario_estadocivil` varchar(45) DEFAULT NULL,
  `usuario_libreta` varchar(50) DEFAULT NULL,
  `usuario_direccion` varchar(200) DEFAULT NULL,
  `usuario_web` varchar(200) DEFAULT NULL,
  `usuario_licenciacond` varchar(50) DEFAULT NULL,
  `usuario_catlicenciacond` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`usuario_id`),
  UNIQUE KEY `usuario_email_UNIQUE` (`usuario_email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Carlos Dias','carlos@email.com','1234','Empleado','245789634','CC','325 547 8546','Soy programador, proactivo.','Colombia','Bogotá','Masculino','Soltero','245789634','cra 70 num 45',NULL,'241545478','B1'),(2,'TecnoCorp','gerente@tecnocorp.com','1234','Empresa','5548976458','NIT','5677987','Empresa dedicada al comercio de tecnologia','Colombia','Bogotá',NULL,NULL,NULL,'cll 158 num 25','www.tecnocorp.com',NULL,NULL),(8,'sdfds','fdgbfdgds@ghfdgsfg.sdf','sdffg','Empresa','sdfsdfsd','NIT','dfgdfg','dfgdfgf','dfgdfg','dfgdfg','TI','','','','','','Sin licencia');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bolsa_empleo_ud'
--

--
-- Dumping routines for database 'bolsa_empleo_ud'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-14  5:20:01
