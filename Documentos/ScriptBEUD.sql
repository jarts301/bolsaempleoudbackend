-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: bolsa_empleo_ud
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aplicacion`
--

DROP TABLE IF EXISTS `aplicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aplicacion` (
  `aplicacion_id` int(11) NOT NULL AUTO_INCREMENT,
  `aplicacion_fecha` datetime NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `oferta_id` int(11) NOT NULL,
  PRIMARY KEY (`aplicacion_id`),
  KEY `fk_Usuario_has_Oferta_Oferta1_idx` (`oferta_id`),
  KEY `fk_Usuario_has_Oferta_Usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_Aplicacion_Oferta` FOREIGN KEY (`oferta_id`) REFERENCES `oferta` (`oferta_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Aplicacion_Usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aplicacion`
--

LOCK TABLES `aplicacion` WRITE;
/*!40000 ALTER TABLE `aplicacion` DISABLE KEYS */;
INSERT INTO `aplicacion` VALUES (6,'2016-11-22 20:01:11',1,1),(9,'2016-11-22 22:56:13',1,3),(10,'2016-11-22 22:59:52',1,1),(11,'2016-11-23 19:39:12',19,3),(12,'2016-11-23 19:39:32',19,4),(13,'2016-11-23 19:47:23',19,1),(14,'2016-11-24 18:21:13',1,3),(15,'2016-11-25 01:00:59',19,5),(16,'2016-11-25 02:26:55',19,1),(17,'2016-11-26 00:36:09',1,3),(18,'2016-11-26 00:41:47',1,3),(19,'2016-11-26 01:07:03',1,4),(20,'2016-11-26 01:29:02',1,1),(21,'2016-11-26 05:22:39',19,1),(22,'2016-11-26 05:53:19',19,6),(23,'2016-11-26 18:44:39',21,3),(24,'2016-11-26 21:08:58',19,6),(25,'2016-11-26 21:09:07',19,6),(26,'2016-11-26 22:12:18',1,1),(27,'2016-11-27 05:45:14',19,7),(28,'2016-12-13 15:26:26',1,1),(29,'2016-12-14 00:26:51',19,1),(30,'2017-02-15 23:18:30',1,1),(31,'2017-02-24 00:23:05',19,1),(32,'2017-02-25 15:16:06',19,4),(33,'2017-02-25 15:16:07',19,4),(34,'2017-02-25 15:16:08',19,4),(35,'2017-02-25 15:18:55',19,4),(36,'2017-02-25 15:18:56',19,4),(37,'2017-02-25 15:18:56',19,4),(38,'2017-02-25 15:18:56',19,4),(39,'2017-02-28 02:43:12',19,12),(40,'2017-03-03 20:50:02',1,3),(41,'2017-03-04 00:49:54',1,15),(42,'2017-03-04 07:29:39',1,3),(43,'2017-03-04 07:43:23',1,1),(44,'2017-03-04 10:19:23',19,5),(45,'2017-03-07 14:58:11',19,15),(46,'2017-03-08 21:40:21',19,5);
/*!40000 ALTER TABLE `aplicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estudio`
--

DROP TABLE IF EXISTS `estudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estudio` (
  `estudio_id` int(11) NOT NULL AUTO_INCREMENT,
  `estudio_nombre` varchar(50) DEFAULT NULL COMMENT 'Profesional, bachiller etc...',
  `estudio_inicio` date DEFAULT NULL,
  `estudio_terminacion` date DEFAULT NULL,
  `estudio_tipo` varchar(200) DEFAULT NULL,
  `estudio_institucion` varchar(200) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`estudio_id`),
  KEY `fk_Estudio_Usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_Estudio_Usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estudio`
--

LOCK TABLES `estudio` WRITE;
/*!40000 ALTER TABLE `estudio` DISABLE KEYS */;
INSERT INTO `estudio` VALUES (2,'Bachiller ','1993-02-01','1998-11-21','Bachiller Académico ','Colegio Carlos Julio Jácome',19),(3,'Ing. Sistemas','2011-02-01','2016-11-30','Ing sistemas','Universidad Nacional',1),(4,'bxjxjx','1982-01-24','1985-02-24','bxjdbdbdb','bxbxbfbfnf',22),(5,'Tecnología Universidad Distrital','2003-03-07','2010-03-07','','Tecnologa Universidad Distrital',19);
/*!40000 ALTER TABLE `estudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experiencia`
--

DROP TABLE IF EXISTS `experiencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiencia` (
  `experiencia_id` int(11) NOT NULL AUTO_INCREMENT,
  `experiencia_empresa` varchar(200) DEFAULT NULL,
  `experiencia_fechainicio` date DEFAULT NULL,
  `experiencia_fechafin` date DEFAULT NULL,
  `experiencia_cargo` varchar(200) DEFAULT NULL,
  `experiencia_funciones` varchar(4000) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `experiencia_contacto` varchar(100) DEFAULT NULL,
  `experiencia_tel_contacto` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`experiencia_id`),
  KEY `fk_Experiencia_Usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_Experiencia_Usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experiencia`
--

LOCK TABLES `experiencia` WRITE;
/*!40000 ALTER TABLE `experiencia` DISABLE KEYS */;
INSERT INTO `experiencia` VALUES (2,'Universidad Distrital','2016-09-26','2016-12-31','Tecnologa','Servicios asistenciales ',19,'.','.'),(3,'Sisco','2012-12-11','2015-07-16','Programador','Desarrollar aplicaciones en java.',1,'.','.'),(4,'savera','1986-07-14','1900-08-24','analista','Hfjf fkdkdkf fjfkfk fjfkfk fjfkfkf jdkdkdj ',22,'.','.'),(5,'yoo','1989-02-24','2000-02-24','jefe','Hxjfkxkx xjfjfjfjf jfjfjfjf jdjdjfjf',22,'.','.'),(6,'hcjfjdkd','1984-02-24','2017-02-24','jxkxjx','Jxkxkx fjdjdjfjf djdkdkdkd djdjdjdjdjd',22,'.','.'),(7,'LandCorp','2017-03-05','2017-03-31','Modista','Msj d djd djd djdjd .....',1,'Pepito','3218765498'),(8,'Saving the Amazon ','2015-01-21','2016-02-07','Gerente Comercial ','Diseñar propuestas para entidades nacionales e internacionales',19,'Ceidy Ruiz ','3005214562');
/*!40000 ALTER TABLE `experiencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oferta`
--

DROP TABLE IF EXISTS `oferta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oferta` (
  `oferta_id` int(11) NOT NULL AUTO_INCREMENT,
  `oferta_cargo` varchar(200) NOT NULL,
  `oferta_salario` varchar(200) NOT NULL,
  `oferta_descripcion` varchar(4000) NOT NULL,
  `oferta_fecha_inicial` datetime NOT NULL,
  `oferta_fecha_final` datetime NOT NULL,
  `oferta_activa` tinyint(1) NOT NULL DEFAULT '1',
  `oferta_pais` varchar(100) NOT NULL,
  `oferta_ciudad` varchar(100) NOT NULL,
  `sector_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`oferta_id`),
  KEY `fk_Oferta_Sector_idx` (`sector_id`),
  KEY `fk_Oferta_Usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_Oferta_Sector` FOREIGN KEY (`sector_id`) REFERENCES `sector` (`sector_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_Oferta_Usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oferta`
--

LOCK TABLES `oferta` WRITE;
/*!40000 ALTER TABLE `oferta` DISABLE KEYS */;
INSERT INTO `oferta` VALUES (1,'Vendedor Tecnologia','1200000','Encargado de la sede de ventas N5, toda la gestión de esta. con 3 o mas años de experiencia en el area. ','2016-11-10 00:00:00','2018-00-00 00:00:00',1,'Colombia','Bogotá DC',1,2),(3,'Programador','2500000','Programador con experiencia en Java.','2016-11-22 21:21:15','2018-00-00 00:00:00',1,'Colombia','Bogotá DC',12,2),(4,'Profesor','800000','Profesor de biología.','2016-11-23 14:30:00','2018-00-00 00:00:00',1,'Colombia','Bogotá DC',17,2),(5,'Almacenista ','1.000.000','Personas que deseen trabajo como almacenistas ','2016-11-25 00:58:44','2018-00-00 00:00:00',1,'Colombia','Bogotá DC',5,20),(6,'Desarrollador de Software ','2.200.000','Se necesita desarrollador de Software con conocimientos en JavaScrip, Linux y Base de datos ','2016-11-26 05:52:06','2018-00-00 00:00:00',1,'Colombia','Bogotá DC',12,20),(7,'Director del departamento de Sistemas ','3.500.000','Dirigir y coordinar el departamento de sistemas de importante empresa de tecnología ','2016-11-27 05:44:02','2018-00-00 00:00:00',1,'Colombia','Bogotá DC',12,20),(8,'Analista redes de comunicaciones','1200000','Bdjfbcldb fjfkfkf dkxjxjf sjdjfnfb','2017-02-25 01:00:19','2018-00-00 00:00:00',1,'Colombia','Bogotá DC',12,23),(9,'secretario ','700000','Bxjx dkxkxn djdkxk xkdjxnx','2017-02-25 01:01:23','2018-00-00 00:00:00',1,'Colombia','Bogotá DC',8,23),(10,'docente ','3450000','Fggjjjjjhhgg','2017-02-25 15:22:33','2016-00-00 00:00:00',0,'Colombia','Bogotá DC',12,23),(11,'agricultor','500000','Hsjsjzjz','2017-02-25 15:59:56','2018-00-00 00:00:00',1,'Colombia','Bogotá DC',7,23),(12,'Gerente Comercial ','3500000','Se requiere gerente Comercial para empresa colombiana','2017-02-28 02:34:54','2018-00-00 00:00:00',1,'Colombia','Bogotá DC',4,27),(13,'jdjsj','10000','Ndnns sjsbs hs sbzj','2017-03-03 22:59:07','2017-03-10 00:00:00',1,'Colombia','Bogotá DC',7,2),(14,'otra vencida','40000000','Jsbhdkdjbdbd','2017-03-03 23:00:23','2017-03-01 00:00:00',0,'Colombia','Bogotá DC',13,2),(15,'mdkdbsv','73641157','Bsbs. djs s s Js. sjsjs','2017-03-04 00:44:47','2017-03-16 00:00:00',1,'COLOMBIA','BARRANQUILLA',20,2),(16,'Soldador ','1500000','Gggggg','2017-03-08 21:47:41','2017-03-13 00:00:00',1,'COLOMBIA','CUCUTA',2,28);
/*!40000 ALTER TABLE `oferta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sector`
--

DROP TABLE IF EXISTS `sector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sector` (
  `sector_id` int(11) NOT NULL AUTO_INCREMENT,
  `sector_codigo` varchar(45) NOT NULL,
  `sector_nombre` varchar(200) NOT NULL,
  PRIMARY KEY (`sector_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sector`
--

LOCK TABLES `sector` WRITE;
/*!40000 ALTER TABLE `sector` DISABLE KEYS */;
INSERT INTO `sector` VALUES (1,'V','Ventas'),(2,'POM','Producción / Operarios / Manufactura'),(3,'A','Atención a clientes'),(4,'AO','Administración / Oficina'),(5,'ALT','Almacén / Logística / Transporte'),(6,'MS','Medicina / Salud'),(7,'SGAS','Servicios Generales, Aseo y Seguridad'),(8,'CO','Construccion y obra'),(9,'CF','Contabilidad / Finanzas'),(10,'CT','CallCenter / Telemercadeo'),(11,'MRT','Mantenimiento y Reparaciones Técnicas'),(12,'IT','Informática / Telecomunicaciones'),(13,'I','Ingeniería'),(14,'HT','Hostelería / Turismo'),(15,'RH','Recursos Humanos'),(16,'MPC','Mercadotécnia / Publicidad / Comunicación'),(17,'D','Docencia'),(18,'DA','Diseño / Artes gráficas'),(19,'IC','Investigación y Calidad'),(20,'CC','Compras / Comercio Exterior'),(21,'LA','Legal / Asesoría'),(22,'DG','Dirección / Gerencia');
/*!40000 ALTER TABLE `sector` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_nombre` varchar(200) NOT NULL,
  `usuario_email` varchar(200) NOT NULL,
  `usuario_pass` varchar(2000) NOT NULL,
  `usuario_rol` varchar(50) NOT NULL COMMENT 'Empresa\nAspirante',
  `usuario_doc` varchar(50) NOT NULL,
  `usuario_tipodoc` varchar(50) NOT NULL,
  `usuario_telefono` varchar(100) NOT NULL,
  `usuario_perfil` varchar(4000) NOT NULL,
  `usuario_pais` varchar(60) DEFAULT NULL,
  `usuario_ciudad` varchar(60) DEFAULT NULL,
  `usuario_genero` varchar(45) DEFAULT NULL,
  `usuario_estadocivil` varchar(45) DEFAULT NULL,
  `usuario_libreta` varchar(50) DEFAULT NULL,
  `usuario_direccion` varchar(200) DEFAULT NULL,
  `usuario_web` varchar(200) DEFAULT NULL,
  `usuario_licenciacond` varchar(50) DEFAULT NULL,
  `usuario_catlicenciacond` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`usuario_id`),
  UNIQUE KEY `usuario_email_UNIQUE` (`usuario_email`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Carlos Dias','jarts301@gmail.com','1234','Aspirante','245789634','CC','325 547 8546','Soy programador, proactivo.','Colombia','Bogotá','Masculino','Soltero','245789634','cra 70 num 45','','241545478','B1'),(2,'TecnoCorp','rahamub301@gmail.com','1234','Empresa','5548976458','NIT','5677987','Empresa dedicada al comercio de tecnologia','Colombia','Bogotá',NULL,NULL,NULL,'cll 158 num 25','www.tecnocorp.com',NULL,NULL),(19,'Lourdes Toro ','lourdes.toro17@gmail.com','miproyecto','Aspirante','52017645 ','CC','3057148518','Hola como estás buscando empleo ','Colombia ','Bogotá ','Femenino','Soltera ','','CRA 34#73B-48','','','Sin licencia'),(20,'Grasco','grasco@grasco.com','grasco','Empresa','20242424','NIT','20242424','Empresa fabricante de productos refinados de aceite','Colombia ','Bogotá ','TI','','','Cra 25#20-20','www.grasco.com','','Sin licencia'),(21,'Us','us@us.com','1234','Aspirante','243424','CC','343221123','fsdfs...','Colombia','Bogotá',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,'jairo Hernández ','jairohg@yahoo.com','Mambo2017','Aspirante','19398071','CC','3006160176','ING de sistemas especializados redes y comunicaciones ','co','bo','Masculino','casado','19398071','cl he f fu dj','https://www.google.com.co/?gfe_rd=cr&ei=CNWwWNKcG9HCgAS33LzQAQ','','Sin licencia'),(23,'jairo Hernández ','jairo.hernandez@saveraservice.com','Mambo@2017','Empresa','830036296-1','NIT','7431818','Bdjdjfjjf fbdjdjdjdk d d d d f s  f f f d f f v  f f f f f djdjfkjdbd jxkxkxjf fjdkxkckf xkfkfjfjfkfkfkfjx xkfkfjxnfjfjfkx fkfkfjfjfjfjf jfkdkdjfjf','ci','bo','Femenino','','','jd d f f f f f','http://www.saveraservice.com','','Sin licencia'),(27,'Servicios de Aseo la Joya','pedronelordones@gmail.com','empresa','Empresa','90052523564','NIT','3057148518','Ofertamos servicios de Aseo ','Colombia ','Bogotá ','Femenino','','','Cra 98#12-56','','','Sin licencia'),(28,'Gerardo ','gerardocastang@gmail.com','proyecto','Empresa','1221121212','NIT','3333333333()','Conductor ','Colombia ','Bogotá ','Femenino','','','Hhhgfghhg','','','Sin licencia');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bolsa_empleo_ud'
--

--
-- Dumping routines for database 'bolsa_empleo_ud'
--
/*!50003 DROP PROCEDURE IF EXISTS `estudio_obtenerPorUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `estudio_obtenerPorUsuario`(in usuarioId int(11))
BEGIN
	select * from estudio where usuario_id = usuarioId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `experiencia_obtenerPorUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `experiencia_obtenerPorUsuario`(in usuarioId int(11))
BEGIN
	select * from experiencia where usuario_id = usuarioId;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `oferta_actualizarActivo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`jarts`@`%` PROCEDURE `oferta_actualizarActivo`()
BEGIN

	update oferta
    set oferta_activa = 0
    where oferta_fecha_final < CURDATE() and oferta_id>0;
    select * from oferta where oferta_id=1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `oferta_obtenerPorSector` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `oferta_obtenerPorSector`(in sectorId int(11))
BEGIN
	if sectorId = 0 then
		select * from oferta where oferta_activa = 1;
    else 
		select * from oferta where sector_id = sectorId and oferta_activa = 1;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sector_obtenerTodos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sector_obtenerTodos`()
BEGIN

	select * from sector;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `usuario_obtenerPorEmailPass` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `usuario_obtenerPorEmailPass`(in email varchar(200), in pass varchar(200))
BEGIN
	select * from usuario where usuario_email = email and usuario_pass = pass;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-09 20:13:10
